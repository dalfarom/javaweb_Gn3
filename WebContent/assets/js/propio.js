$(document).ready(function(){
	
	/* Ocultar todos los ejercicios */
	$("#ejer1").hide();
	$("#ejer2").hide();
	$("#ejer3").hide();
	$("#ejer4").hide();
	$("#ejer5").hide();
	$("#ejer6").hide();
	$("#ejer7").hide();
	$("#ejer8").hide();
	$("#ejer9").hide();
	$("#ejer10").hide();
	$("#bienvenido").show();
	/* Fin */
	
	/* Funcion para cambiar el class "menu"  para cambiar color*/
	$('a.menu').click(function(){
	    $('a.menu').removeClass("active");
	    $(this).addClass("active");
	});
	/* Fin */
	
	/* Funciones para mostrar ejercicio con click en menu */
	$("#boton1").click(function() {
		$("#ejer1").show();
		$("#ejer2").hide();
		$("#ejer3").hide();
		$("#ejer4").hide();
		$("#ejer5").hide();
		$("#ejer6").hide();
		$("#ejer7").hide();
		$("#ejer8").hide();
		$("#ejer9").hide();
		$("#ejer10").hide();
		$("#bienvenido").hide();
	});
	$("#boton2").click(function() {
		$("#ejer1").hide();
		$("#ejer2").show();
		$("#ejer3").hide();
		$("#ejer4").hide();
		$("#ejer5").hide();
		$("#ejer6").hide();
		$("#ejer7").hide();
		$("#ejer8").hide();
		$("#ejer9").hide();
		$("#ejer10").hide();
		$("#bienvenido").hide();
	});
	$("#boton3").click(function() {
		$("#ejer1").hide();
		$("#ejer2").hide();
		$("#ejer3").show();
		$("#ejer4").hide();
		$("#ejer5").hide();
		$("#ejer6").hide();
		$("#ejer7").hide();
		$("#ejer8").hide();
		$("#ejer9").hide();
		$("#ejer10").hide();
		$("#bienvenido").hide();
	});
	$("#boton4").click(function() {
		$("#ejer1").hide();
		$("#ejer2").hide();
		$("#ejer3").hide();
		$("#ejer4").show();
		$("#ejer5").hide();
		$("#ejer6").hide();
		$("#ejer7").hide();
		$("#ejer8").hide();
		$("#ejer9").hide();
		$("#ejer10").hide();
		$("#bienvenido").hide();
	});
	$("#boton5").click(function() {
		$("#ejer1").hide();
		$("#ejer2").hide();
		$("#ejer3").hide();
		$("#ejer4").hide();
		$("#ejer5").show();
		$("#ejer6").hide();
		$("#ejer7").hide();
		$("#ejer8").hide();
		$("#ejer9").hide();
		$("#ejer10").hide();
		$("#bienvenido").hide();
	});
	$("#boton6").click(function() {
		$("#ejer1").hide();
		$("#ejer2").hide();
		$("#ejer3").hide();
		$("#ejer4").hide();
		$("#ejer5").hide();
		$("#ejer6").show();
		$("#ejer7").hide();
		$("#ejer8").hide();
		$("#ejer9").hide();
		$("#ejer10").hide();
		$("#bienvenido").hide();
	});
	$("#boton7").click(function() {
		$("#ejer1").hide();
		$("#ejer2").hide();
		$("#ejer3").hide();
		$("#ejer4").hide();
		$("#ejer5").hide();
		$("#ejer6").hide();
		$("#ejer7").show();
		$("#ejer8").hide();
		$("#ejer9").hide();
		$("#ejer10").hide();
		$("#bienvenido").hide();
	});
	$("#boton8").click(function() {
		$("#ejer1").hide();
		$("#ejer2").hide();
		$("#ejer3").hide();
		$("#ejer4").hide();
		$("#ejer5").hide();
		$("#ejer6").hide();
		$("#ejer7").hide();
		$("#ejer8").show();
		$("#ejer9").hide();
		$("#ejer10").hide();
		$("#bienvenido").hide();
	});
	$("#boton9").click(function() {
		$("#ejer1").hide();
		$("#ejer2").hide();
		$("#ejer3").hide();
		$("#ejer4").hide();
		$("#ejer5").hide();
		$("#ejer6").hide();
		$("#ejer7").hide();
		$("#ejer8").hide();
		$("#ejer9").show();
		$("#ejer10").hide();
		$("#bienvenido").hide();
	});
	
	$("#boton10").click(function() {
		$("#ejer1").hide();
		$("#ejer2").hide();
		$("#ejer3").hide();
		$("#ejer4").hide();
		$("#ejer5").hide();
		$("#ejer6").hide();
		$("#ejer7").hide();
		$("#ejer8").hide();
		$("#ejer9").hide();
		$("#ejer10").show();
		$("#bienvenido").hide();
	});

	
});