
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Guia N�3 Java Web</title>

<!-- CSS's -->

	<link rel="stylesheet" type="text/css" href="./assets/css/bootstrap.css">

<!-- Fin -->

</head>
<body>
	<!-- Header -->
	
	<div class="jumbotron jumbotron-fluid">
  		<div class="container">
    		<h1 class="display-4">Guia de Ejercicios N�3</h1>
    		<!-- Menu -->
			<nav class="nav nav-pills flex-column flex-sm-row">
				<a class="flex-sm-fill text-sm-center nav-link menu" id="boton1">Ejercicio 1</a>
				<a class="flex-sm-fill text-sm-center nav-link menu" id="boton2">Ejercicio 2</a>
				<a class="flex-sm-fill text-sm-center nav-link menu" id="boton3">Ejercicio 3</a>
				<a class="flex-sm-fill text-sm-center nav-link menu" id="boton4">Ejercicio 4</a>
				<a class="flex-sm-fill text-sm-center nav-link menu" id="boton5">Ejercicio 5</a>
				<a class="flex-sm-fill text-sm-center nav-link menu" id="boton6">Ejercicio 6</a>
				<a class="flex-sm-fill text-sm-center nav-link menu" id="boton7">Ejercicio 7</a>
				<a class="flex-sm-fill text-sm-center nav-link menu" id="boton8">Ejercicio 8</a>
				<a class="flex-sm-fill text-sm-center nav-link menu" id="boton9">Ejercicio 9</a>
				<a class="flex-sm-fill text-sm-center nav-link menu" id="boton10">Ejercicio 10</a>
			</nav>
	<!-- Fin Menu -->
  		</div>
	</div>
	
	<div id="bienvenido">
		<h1 class="display-4"><center>Bienvenido Seleccione una Opci�n</center></h1>
	</div>
	
	<div id="ejer1" class="col-md 10">
	
	<h1 class="display-4">Ejercicio 1</h1>
	
	<!-- Formulario para revisar Registros -->
	<form action="ConsultasDB" method="post">
		<input type="hidden" name="ejercicio" value="1">
		<button type="submit" class="btn btn-info" style="float: right; margin-bottom: 11px;">Ver Historial de Registros</button>
	</form>
	<!-- Fin -->
		<table class="table">
  			<thead class="thead-dark">
    		<tr>
      			<th >Vendedor</th>
      			<th >Venta</th>
      			<th >Sueldo</th>
      			<th >Acci�n</th>
    		</tr>
  			</thead>
  			<tbody>
    			<tr>
    			<form action="Ejercicio1" method="POST" name="ejer1Form">
      				<td><input type="text" class="form-control" id="inVendedor" name="inVendedor" placeholder="Nombre" required></td>
      				<td><input type="number" class="form-control" id="inVenta" min=0 name="inVenta" placeholder="Monto Pesos" required></td>
      				<td><input type="number" class="form-control" id="inSueldo" min=0 name="inSueldo" placeholder="Monto Pesos" required></td>
      				<td><button type="submit" class="btn btn-success" style="width: 100%;">Ingresar Datos</button></td>
      			</form>
    			</tr>
  			</tbody>
		</table>
		
	</div>
	
	<div id="ejer2" class="col-md 10">
	
	<h1 class="display-4">Ejercicio 2</h1>
	<form action="ConsultasDB" method="post">
		<input type="hidden" name="ejercicio" value="2">
		<button type="submit" class="btn btn-info" style="float: right; margin-bottom: 11px;">Ver Historial de Registros</button>
	</form>
		<table class="table">
  			<thead class="thead-dark">
    		<tr>
      			<th scope="col">Nombre</th>
      			<th scope="col">Edad</th>
      			<th >Acci�n</th>
      			
    		</tr>
  			</thead>
  			<tbody>
    			<tr>
    			<form action="Ejercicio2" method="POST">
      				<td><input type="text" required class="form-control" id="idNombre" name="idNombre" placeholder="Nombre"></td>
      				<td><input type="number" required class="form-control" id="idEdad" name="idEdad" min=0 placeholder="Edad"></td>
      				
      				<td><button type="submit" class="btn btn-success" style="width: 100%;">Ingresar Datos</button></td>
      			</form>
    			</tr>
  			</tbody>
		</table>
	</div>
	
	<div id="ejer3" class="col-md 10">
	
	<h1 class="display-4">Ejercicio 3</h1>
	
		<table class="table">
  			<thead class="thead-dark">
    		<tr>
      			<th scope="col">Nombre</th>
      			<th scope="col">Venta</th>
      			<th >Acci�n</th>
    		</tr>
  			</thead>
  			<tbody>
    			<tr>
    			<form action="Ejercicio3" method="POST">
      				<td><input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre"></td>
      				<td><input type="number" class="form-control" id="venta" name="venta" min=0 placeholder="Venta"></td>
      				<td><button type="submit" class="btn btn-success" style="width: 100%;">Ingresar Datos</button></td>
      			</form>
    			</tr>
  			</tbody>
		</table>
	</div>
	
	<div id="ejer4" class="col-md 10">
	
	<h1 class="display-4">Ejercicio 4</h1>
	
		<table class="table">
  			<thead class="thead-dark">
    		<tr>
      			<th scope="col">Vendedor</th>
      			<th scope="col">Venta (UF)</th>
      			<th scope="col">Sueldo</th>
      			<th scope="col">Acci�n</th>
    		</tr>
  			</thead>
  			<tbody>
    			<tr>
    			<form action="Ejercicio4" method="POST" name="ejer4Form">
      				<td><input type="text" required class="form-control" id="inVendedor" name="inVendedor" placeholder="Nombre"></td>
      				<td><input type="number" required class="form-control" id="inVenta" min=0 name="inVenta" placeholder="Monto UF"></td>
      				<td><input type="number" required class="form-control" id="inSueldo" min=0 name="inSueldo" placeholder="Monto Pesos"></td>
      				<td><button type="submit"  class="btn btn-success" style="width: 100%;">Ingresar Datos</button></td>
      			</form>
    			</tr>
  			</tbody>
		</table>
	</div>
	
	<div id="ejer5" class="col-md 10">
	
	<h1 class="display-4">Ejercicio 5</h1>
		<form action="ConsultasDB" method="POST">
			<input type="hidden" name="ejercicio" value="5">
			<button type="submit" class="btn btn-info" style="float: right; margin-bottom: 11px;">Ver Historial de Registros</button>
		</form>
		<table class="table">
  			<thead class="thead-dark">
    		<tr>
      			<th>Vendedor</th>
      			<th>Sueldo (UF)</th>
      			<th>Ingreso (Mes)</th>
      			<th >Acci�n</th>
    		</tr>
  			</thead>
  			<tbody>
    			<tr>
    			<form action="Ejercicio5" method="POST" name="ejer5Form">
      				<td><input type="text" class="form-control" id="inVendedor" name="inVendedor" placeholder="Nombre" required></td>
      				<td><input type="number" class="form-control" id="inSueldo" min=0 name="inSueldo" placeholder="Monto UF" required></td>
      				<td>
      					<select class="custom-select" id="selectMeses" name="inIngreso" required>
    						<!--  <option selected>Seleccion Mes...</option> -->
    						<option value="1">Enero</option>
    						<option value="2">Febrero</option>
    						<option value="3">Marzo</option>
    						<option value="4">Abril</option>
    						<option value="5">Mayo</option>
    						<option value="6">Junio</option>
    						<option value="7">Julio</option>
    						<option value="8">Agosto</option>
    						<option value="9">Septiembre</option>
    						<option value="10">Octubre</option>
    						<option value="11">Noviembre</option>
    						<option value="12">Diciembre</option>
  						</select>
  					</td>
      				<td><button type="submit" class="btn btn-success" style="width: 100%;">Ingresar Datos</button></td>
      			</form>
    			</tr>
  			</tbody>
		</table>
	</div>
	
	<div id="ejer6" class="col-md 10">
	<h1 class="display-4">Ejercicio 6</h1>
		
		<table class="table">
  			<thead class="thead-dark">
    		<tr>
      			<th scope="col">Vendedor</th>
      			<th scope="col">Rendimiento</th>
      			<th >Acci�n</th>
      		
    		</tr>
  			</thead>
  			<tbody>
    			<tr>
    			<form action="Ejercicio6" method="POST">
      				<td><input type="text" class="form-control" required id="inNombre" name="inNombre" placeholder="Nombre"></td>
      				<td><input type="number" max="100" required min=0 class="form-control" id="inPorcentaje" name="inPorcentaje" placeholder="Porcentaje"></td>
      				<td><button type="submit" class="btn btn-success" style="width: 100%;">Ingresar Datos</button></td>
      			</form>
    			</tr>
  			</tbody>
		</table>
	</div>
	
	<div id="ejer7" class="col-md 10">
	<h1 class="display-4">Ejercicio 7</h1>
		<table class="table">
  			<thead class="thead-dark">
    		<tr>
      			<th scope="col">Producto</th>
      			<th scope="col">Precio</th>
      			<th scope="col">Categoria</th>
      			<th >Acci�n</th>
      			
      			
    		</tr>
  			</thead>
  			<tbody>
    			<tr>
    			<form action="Ejercicio7" method="POST">
      				<td><input type="text" class="form-control" required id="inProducto" name="inProducto" placeholder="Producto"></td>
      				<td><input type="number" class="form-control" min=0 required id="inPrecio" name="inPrecio" placeholder="Precio"></td>
      				<td><select class="custom-select" required name="inCategoria" required>
    						<option value="A">Categoria A</option>
    						<option value="B">Categoria B</option>
  						</select></td>
      				<td><button type="submit" class="btn btn-success" style="width: 100%;">Ingresar Datos</button></td>
      			</form>
    			</tr>
  			</tbody>
		</table>
	</div>
	
	<div id="ejer8" class="col-md 10">
	<h1 class="display-4">Ejercicio 8</h1>
	
		<table class="table">
  			<thead class="thead-dark">
    		<tr>
      			<th scope="col">Partido</th>
      			<th scope="col">Tenista 1</th>
      			<th scope="col">Set Ganados</th>
      			<th scope="col">Tenista 2</th>
      			<th scope="col">Set Ganados</th>
      			<th >Acci�n</th>
    		</tr>
  			</thead>
  			<tbody>
    			<tr>
    			<form action="Ejercicio8" method="POST">
      				<td><input type="number" class="form-control" required min=0 id="partido" name="partido" placeholder="Partido"></td>
      				<td><input type="text" class="form-control" required id="tenista1" name="tenista1" placeholder="Nombre"></td>
      				<td><input type="number" class="form-control" required min=0 id="setTeni1" name="setTeni1" placeholder="Set ganados"></td>
      				<td><input type="text" class="form-control" required id="tenista2" name="tenista2" placeholder="Nombre"></td>
      				<td><input type="number" class="form-control" required min=0 id="setTeni2" name="setTeni2" placeholder="Set ganados"></td>
      				<td><button type="submit" class="btn btn-success" style="width: 100%;">Ingresar Datos</button></td>
      			</form>
    			</tr>
  			</tbody>
		</table>
	</div>	

	
	<div id="ejer9"	class="col-md 10">
	
	<h1 class="display-4">Ejercicio 9</h1>
	<!-- Formulario para revisar Registros -->
	<form action="ConsultasDB" method="post">
		<input type="hidden" name="ejercicio" value="9">
		<button type="submit" class="btn btn-info" style="float: right; margin-bottom: 11px;">Ver Historial de Registros</button>
	</form>
	<!-- Fin -->
		<table class="table">
  			<thead class="thead-dark">
    		<tr>
      			<th scope="col">Empresa</th>
      			<th scope="col">Ganancia</th>
      			<th >Acci�n</th>
      			
    		</tr>
  			</thead>
  			<tbody>
    			<tr>
    			<form action="Ejercicio9" method="POST" name="ejer9Form">
      				<td><input type="text" class="form-control" id="idNombre" required name="idNombre" placeholder="Empresa"></td>
      				<td><input type="number" class="form-control" min=0 id="idGanancia" required name="idGanancia" placeholder="Ganancia"></td>
      				
      				<td><button type="submit" class="btn btn-success" style="width: 100%;">Ingresar Datos</button></td>
      			</form>
    			</tr>
  			</tbody>
		</table>
	</div>
	
	<div id="ejer10" class="col-md 10">
		<h1 class="display-4">Ejercicio 10</h1>
		
		<form action="ConsultasDB" method="POST">
			<input type="hidden" name="ejercicio" value="10">
			<button type="submit" class="btn btn-info" style="float: right; margin-bottom: 11px;">Ver Historial de Registros</button>
		</form>
			<table class="table">
  				<thead class="thead-dark">
    				<tr>
      					<th scope="col">Nombre</th>
      					<th scope="col">Venta</th>
      					<th >Acci�n</th>
    				</tr>
  				</thead>
  				<tbody>
    				<tr>
    					<form action="Ejercicio10" method="POST">
      						<td><input type="text" required class="form-control" id="idNombre" name="idNombre" placeholder="Nombre"></td>
      						<td><input type="number" required class="form-control" id="idVenta" name="idVenta" min=0 placeholder="Venta"></td>
      						<td><button type="submit" class="btn btn-success" style="width: 100%;">Ingresar Datos</button></td>
      					</form>
    				</tr>
  				</tbody>
			</table>
	</div>
	
	<!-- Fin Header -->
	
	<script type="text/javascript" src="./assets/js/bootstrap.js"></script>
	<script type="text/javascript" src="./assets/js/jquery.js"></script>
	<script type="text/javascript" src="./assets/js/propio.js"></script>
</body>
</html>