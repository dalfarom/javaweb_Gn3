package controlladores;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;



/**
 * Servlet implementation class Ejercicio2
 */
@WebServlet("/Ejercicio2")
public class Ejercicio2 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Ejercicio2() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		
		response.setContentType("text/html");
		PrintWriter out=response.getWriter();
		
		String Nombre = request.getParameter("idNombre");
		int Edad = Integer.parseInt(request.getParameter("idEdad"));
				
		// Ejecicion Logica
		
		String resultado = null;
		
		if(Edad < 18) {
			  resultado = "Menor de Edad";
		}else {
			resultado = "Mayor de Edad";
		}
		
		//instanciacion de la clase mysql para conectar a la DB
		
		mysql conexion = new mysql("guian3javaweb","root","");
				
		try {
			Statement st = conexion.conexion().createStatement();
					
			st.executeUpdate("insert into ejercicio2 (id, nombre, edad, condicion) values(null,'"+Nombre+"',"+Edad+",'"+resultado+"')");
					
						
		} catch (SQLException e) {
					
			e.printStackTrace();
		}
				
		try {
			conexion.conexion().close();
		} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		}
				
				
		// Retorno al JSP
		response.sendRedirect("index.jsp");
						
		}
	

}
