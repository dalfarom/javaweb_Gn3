package controlladores;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class mysql {
	public mysql(String nombreBaseDatos, String usuario, String contrasena) {
		this.nombreBaseDatos = nombreBaseDatos;
		this.usuario = usuario;
		this.contrasena = contrasena;
	}

	private String nombreBaseDatos = "guian3javaweb";
	private String url = "jdbc:mysql://localhost:3306/"+nombreBaseDatos+"?autoReconnect=true&useSSL=false";
	private Connection con;
	private Statement st;
	private ResultSet rs;
	private String usuario ="root";
	private String contrasena ="";
	
	public Connection conexion() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			
			con = DriverManager.getConnection(url,usuario, contrasena);
			
		} catch (ClassNotFoundException e) {
			
			e.printStackTrace();
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		
		return con;
	}
	
	
	public mysql() {
		
	}


	public String getNombreBaseDatos() {
		return nombreBaseDatos;
	}


	public void setNombreBaseDatos(String nombreBaseDatos) {
		this.nombreBaseDatos = nombreBaseDatos;
	}


	public String getUsuario() {
		return usuario;
	}


	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}


	public String getContrasena() {
		return contrasena;
	}


	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}


	public Statement getSt() {
		return st;
	}


	public void setSt(Statement st) {
		this.st = st;
	}


	public ResultSet getRs() {
		return rs;
	}


	public void setRs(ResultSet rs) {
		this.rs = rs;
	}
	
	
}
