package controlladores;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;

/**
 * Servlet implementation class Ejercicio4
 */
@WebServlet("/Ejercicio4")
public class Ejercicio4 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Ejercicio4() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		// Lineas de Codigo Obligatorias
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		// Recibiendo inputs del Formualrio y alamcenandolas en variables locales
		String Nombre = request.getParameter("inVendedor");
		int UF = Integer.parseInt(request.getParameter("inVenta"));
		int Sueldo = Integer.parseInt(request.getParameter("inSueldo"));
		
		// Ejercicion Logica
		double resultado = 0;
		
		if(UF >= 50) {
			 resultado = Sueldo+(Sueldo*0.20);
		}else {
			resultado = Sueldo+(Sueldo*0.10);
		}
		
		resultado = (int)resultado;
		
		// Impresion de variables
		out.println("Estimado :"+Nombre+", su reasultado es :"+resultado);
		
	}

}
