package controlladores;
import java.io.PrintWriter;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Ejercicio8
 */
@WebServlet("/Ejercicio8")
public class Ejercicio8 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Ejercicio8() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		
		// Lineas de Codigo Obligatorias
		response.setContentType("text/html");
		PrintWriter out=response.getWriter();
		
		// Recibiendo inputs del Formualrio y alamcenandolas en variables locales
		
		int partido = Integer.parseInt(request.getParameter("partido"));
		String tenista1 = request.getParameter("tenista1");
		int setTeni1 = Integer.parseInt(request.getParameter("setTeni1"));
		String tenista2 = request.getParameter("tenista2");
		int setTeni2 = Integer.parseInt(request.getParameter("setTeni2"));
	
		String ganador = null;
		
		if(setTeni1 > setTeni2) {
			ganador = tenista1;
		}else {
			ganador = tenista2;
		}
		
		out.print("El ganador es: "+ganador);
	}

}
