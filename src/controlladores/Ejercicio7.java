package controlladores;

import java.io.PrintWriter;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Ejercicio7
 */
@WebServlet("/Ejercicio7")
public class Ejercicio7 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Ejercicio7() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		
		// Lineas de Codigo Obligatorias
		response.setContentType("text/html");
		PrintWriter out=response.getWriter();
		
		//Ejercicio 7
		
		String Producto = request.getParameter("inProducto");
		int Precio = Integer.parseInt(request.getParameter("inPrecio"));
		String Categoria = (request.getParameter("inCategoria"));
		
		// Ejecicion Logica
		double resultado = 0;
		
		if(Categoria == "A") {
			 resultado = Precio-(Precio*0.05);
		}else {
			resultado = Precio-(Precio*0.10);
		}
		
		
		
		// Impresion de variables
		out.println("Estimado su precio final es :"+resultado);
		
	}

}
