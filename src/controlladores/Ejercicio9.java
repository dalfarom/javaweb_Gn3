package controlladores;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Servlet implementation class Ejercicio9
 */
@WebServlet("/Ejercicio9")
public class Ejercicio9 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Ejercicio9() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		// response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		// doGet(request, response);
		
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		String Nombre = request.getParameter("idNombre");
		Float Ganancia = Float.parseFloat(request.getParameter("idGanancia"));
				
		// Ejecicion Logica
		float Impuesto = 0;
		
		if(Ganancia <= 50000000) {
			Impuesto =  Ganancia*19/100;			  
		}else {
			Impuesto =  Ganancia*22/100;	
		}
		
		//instanciacion de la clase mysql para conectar a la DB
		
		mysql conexion = new mysql("guian3javaweb","root","");
				
		try {
			Statement st = conexion.conexion().createStatement();
					
			st.executeUpdate("insert into ejercicio9 (id, empresa, ganancia, impuesto) values(null,'"+Nombre+"',"+Ganancia+","+Impuesto+")");
					
						
		} catch (SQLException e) {
					
			e.printStackTrace();
		}
				
		try {
			conexion.conexion().close();
		} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		}
				
				
		// Retorno al JSP
			response.sendRedirect("index.jsp");
						
		}
	
		


}
