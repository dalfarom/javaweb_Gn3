package controlladores;
import java.io.PrintWriter;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Ejercicio6
 */
@WebServlet("/Ejercicio6")
public class Ejercicio6 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Ejercicio6() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		// response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		// doGet(request, response);
		
		response.setContentType("text/html");
		PrintWriter out=response.getWriter();
		
		// Recibiendo inputs del Formualrio y alamcenandolas en variables locales
		String Nombre = request.getParameter("inNombre");
		int Porcentaje = Integer.parseInt(request.getParameter("inPorcentaje"));
		
		
		// Ejecicion Logica
	
		
		if(Porcentaje >= 70) {
			out.println("Estimado :"+Nombre+", Muy Bien");
		}else {
			out.println("Estimado :"+Nombre+", debe superarse ");
		}
		
	}

}
