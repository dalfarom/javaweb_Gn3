package controlladores;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Servlet implementation class Ejercicio1
 */
@WebServlet("/Ejercicio1")
public class Ejercicio1 extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Ejercicio1() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		
		// Lineas de Codigo Obligatorias
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
				
		// Recibiendo inputs del Formualrio y alamcenandolas en variables locales
		String Nombre = request.getParameter("inVendedor");
		int Venta = Integer.parseInt(request.getParameter("inVenta"));
		int Sueldo = Integer.parseInt(request.getParameter("inSueldo"));
		
		int resultado = 0;
		
		//Logica
		if(Venta >= 1000) {
			resultado = Sueldo*2;
		}else {
			resultado = Sueldo;
		}
		
		
		//instanciacion de la clase mysql para conectar a la DB
		
		mysql conexion = new mysql("guian3javaweb","root","");
		
		try {
			Statement st = conexion.conexion().createStatement();
			
			st.executeUpdate("insert into ejercicio1 (id, vendedor, venta, sueldo, sueldoFinal) values(null,'"+Nombre+"',"+Venta+","+Sueldo+","+resultado+")");
			
			
			
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		
		try {
			conexion.conexion().close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		// Retorno al JSP
		response.sendRedirect("index.jsp");
				
	}

}
