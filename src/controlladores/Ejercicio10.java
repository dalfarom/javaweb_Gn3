package controlladores;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


/**
 * Servlet implementation class Ejercicio10
 */
@WebServlet("/Ejercicio10")
public class Ejercicio10 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Ejercicio10() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		
		// Lineas de Codigo Obligatorias
				response.setContentType("text/html");
				PrintWriter out = response.getWriter();
						
				// Recibiendo inputs del Formualrio y alamcenandolas en variables locales
				String Nombre = request.getParameter("idNombre");
				int Venta = Integer.parseInt(request.getParameter("idVenta"));
				
				int Sueldo = 150000;
				
				int resultado = 0;
				
				int porcentaje = Venta*10 / 100;
				
				//Logica
				if(Venta <= 300000) {
					resultado = Sueldo;
				}else {
					resultado = porcentaje + Sueldo;
					
				}
				
				
				
				//instanciacion de la clase mysql para conectar a la DB
				
				mysql conexion = new mysql("guian3javaweb","root","");
				
				try {
					Statement st = conexion.conexion().createStatement();
					
					st.executeUpdate("insert into ejercicio10 (id, nombre, ventas, sueldo) values(null,'"+Nombre+"',"+Venta+","+resultado+")");
					
					
					
				} catch (SQLException e) {
					
					e.printStackTrace();
				}
				
				try {
					conexion.conexion().close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
				// Retorno al JSP
				response.sendRedirect("index.jsp");
						
				

	}

}
