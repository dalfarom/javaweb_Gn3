package controlladores;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.sql.Array;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Servlet implementation class ConsultasDB
 */
@WebServlet("/ConsultasDB")
public class ConsultasDB extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ConsultasDB() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		
		// Lineas de Codigo Obligatorias
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
				
		// Recibiendo inputs del Formualrio y alamcenandolas en variables locales
		int ejercicio = Integer.parseInt(request.getParameter("ejercicio"));
		
		//instanciacion de la clase mysql para conectar a la DB
		
		mysql conexion = new mysql("guian3javaweb","root","");
		
		try {
			Statement st = conexion.conexion().createStatement();
			
			ResultSet select = st.executeQuery("select * from ejercicio"+ejercicio);
			
			switch (ejercicio) {
			case 1:
				out.println("<!doctype html>");
				out.println("<html lang=\"en\">");
				out.println("<head>");
				out.println("<meta charset=\"utf-8\">");
				out.println("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">");
				out.println("<link rel=\"stylesheet\" type=\"text/css\" href=\"./assets/css/bootstrap.css\">");
				out.println("<title>Guia N�3 Java Web</title>");
				out.println("</head>");
				out.println("<body>");
				out.println("<table class=\"table\">");
				out.println("<thead class=\"thead-dark\">");
				out.println("<tr>");
				out.println("<th>Vendedor</th>");
				out.println("<th>Venta</th>");
				out.println("<th>Sueldo</th>");
				out.println("<th>Sueldo Final</th>");
				out.println("</tr>");
				out.println("</thead>");
				out.println("<tbody>");
				out.println("");
				while(select.next()) {
					out.println("<tr><td>"+select.getObject("vendedor")+"</td>");
					out.println("<td>"+select.getObject("venta")+"</td>");
					out.println("<td>"+select.getObject("sueldo")+"</td>");
					out.println("<td>"+select.getObject("sueldoFinal")+"</td></tr>");
				}
				
				out.println("</tbody>");
				out.println("</table>");
				out.println("<script type=\"text/javascript\" src=\"./assets/js/bootstrap.js\"></script>");
				out.println("<script type=\"text/javascript\" src=\"./assets/js/jquery.js\"></script>");
				out.println("<script type=\"text/javascript\" src=\"./assets/js/propio.js\"></script>");
				out.println("</body>");
				out.println("</html>");
				break;
				
			case 2:
				
				out.println("<!doctype html>");
				out.println("<html lang=\"en\">");
				out.println("<head>");
				out.println("<meta charset=\"utf-8\">");
				out.println("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">");
				out.println("<link rel=\"stylesheet\" type=\"text/css\" href=\"./assets/css/bootstrap.css\">");
				out.println("<title>Guia N�3 Java Web</title>");
				out.println("</head>");
				out.println("<body>");
				out.println("<table class=\"table\">");
				out.println("<thead class=\"thead-dark\">");
				out.println("<tr>");
				out.println("<th>Nombre</th>");
				out.println("<th>Edad</th>");
				out.println("<th>Condicion</th>");
				out.println("</tr>");
				out.println("</thead>");
				out.println("<tbody>");
				out.println("");
				while(select.next()) {
					out.println("<tr><td>"+select.getObject("nombre")+"</td>");
					out.println("<td>"+select.getObject("edad")+"</td>");
					out.println("<td>"+select.getObject("condicion")+"</td></tr>");
				}
				
				out.println("</tbody>");
				out.println("</table>");
				out.println("<script type=\"text/javascript\" src=\"./assets/js/bootstrap.js\"></script>");
				out.println("<script type=\"text/javascript\" src=\"./assets/js/jquery.js\"></script>");
				out.println("<script type=\"text/javascript\" src=\"./assets/js/propio.js\"></script>");
				out.println("</body>");
				out.println("</html>");
				
				break;
				
			case 3:
				
				break;
			case 4:
				
				break;
				
			case 5:
				
				out.println("<!doctype html>");
				out.println("<html lang=\"en\">");
				out.println("<head>");
				out.println("<meta charset=\"utf-8\">");
				out.println("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">");
				out.println("<link rel=\"stylesheet\" type=\"text/css\" href=\"./assets/css/bootstrap.css\">");
				out.println("<title>Guia N�3 Java Web</title>");
				out.println("</head>");
				out.println("<body>");
				out.println("<table class=\"table\">");
				out.println("<thead class=\"thead-dark\">");
				out.println("<tr>");
				out.println("<th>Vendedor</th>");
				out.println("<th>Sueldo (UF)</th>");
				out.println("<th>Mes de Ingerso</th>");
				out.println("<th>Sueldo Final (UF)</th>");
				out.println("</tr>");
				out.println("</thead>");
				out.println("<tbody>");
				out.println("");
				while(select.next()) {
					out.println("<tr><td>"+select.getObject("vendedor")+"</td>");
					out.println("<td>"+select.getObject("sueldo_uf")+"</td>");
					out.println("<td>"+select.getObject("ingresoMes")+"</td>");
					out.println("<td>"+select.getObject("sueldoFinal_uf")+"</td></tr>");
				}
				
				out.println("</tbody>");
				out.println("</table>");
				out.println("<script type=\"text/javascript\" src=\"./assets/js/bootstrap.js\"></script>");
				out.println("<script type=\"text/javascript\" src=\"./assets/js/jquery.js\"></script>");
				out.println("<script type=\"text/javascript\" src=\"./assets/js/propio.js\"></script>");
				out.println("</body>");
				out.println("</html>");
				
				break;
				
			case 6:
				break;
				
			case 7:
				break;
				
			case 8:
				break;
				
			case 9:
				
				out.println("<!doctype html>");
				out.println("<html lang=\"en\">");
				out.println("<head>");
				out.println("<meta charset=\"utf-8\">");
				out.println("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">");
				out.println("<link rel=\"stylesheet\" type=\"text/css\" href=\"./assets/css/bootstrap.css\">");
				out.println("<title>Guia N�3 Java Web</title>");
				out.println("</head>");
				out.println("<body>");
				out.println("<table class=\"table\">");
				out.println("<thead class=\"thead-dark\">");
				out.println("<tr>");
				out.println("<th>Nombre</th>");
				out.println("<th>Ganancia</th>");
				out.println("<th>Impuesto</th>");
				out.println("</tr>");
				out.println("</thead>");
				out.println("<tbody>");
				out.println("");
				while(select.next()) {
					out.println("<tr><td>"+select.getObject("empresa")+"</td>");
					out.println("<td>"+select.getObject("ganancia")+"</td>");
					out.println("<td>"+select.getObject("impuesto")+"</td></tr>");
				}
				
				out.println("</tbody>");
				out.println("</table>");
				out.println("<script type=\"text/javascript\" src=\"./assets/js/bootstrap.js\"></script>");
				out.println("<script type=\"text/javascript\" src=\"./assets/js/jquery.js\"></script>");
				out.println("<script type=\"text/javascript\" src=\"./assets/js/propio.js\"></script>");
				out.println("</body>");
				out.println("</html>");
				
				break;
			case 10:
				
				out.println("<!doctype html>");
				out.println("<html lang=\"en\">");
				out.println("<head>");
				out.println("<meta charset=\"utf-8\">");
				out.println("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">");
				out.println("<link rel=\"stylesheet\" type=\"text/css\" href=\"./assets/css/bootstrap.css\">");
				out.println("<title>Guia N�3 Java Web</title>");
				out.println("</head>");
				out.println("<body>");
				out.println("<table class=\"table\">");
				out.println("<thead class=\"thead-dark\">");
				out.println("<tr>");
				out.println("<th>Nombre</th>");
				out.println("<th>Venta</th>");
				out.println("<th>resultado</th>");
				out.println("</tr>");
				out.println("</thead>");
				out.println("<tbody>");
				out.println("");
				while(select.next()) {
					out.println("<tr><td>"+select.getObject("nombre")+"</td>");
					out.println("<td>"+select.getObject("ventas")+"</td>");
					out.println("<td>"+select.getObject("sueldo")+"</td></tr>");
				}
				
				out.println("</tbody>");
				out.println("</table>");
				out.println("<script type=\"text/javascript\" src=\"./assets/js/bootstrap.js\"></script>");
				out.println("<script type=\"text/javascript\" src=\"./assets/js/jquery.js\"></script>");
				out.println("<script type=\"text/javascript\" src=\"./assets/js/propio.js\"></script>");
				out.println("</body>");
				out.println("</html>");
				
				break;
			default:
				out.print("No existe la opci�n");
				break;
			}
			
			
			
				
			
			
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		
		try {
			conexion.conexion().close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
